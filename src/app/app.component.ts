import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { FetchingAlertService } from './services/fetching-alert.service';
import { PersonListsService } from './services/person-lists.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'sete-graus-exercise';

  /**
   * The array to store the person lists
   *
   * @memberof AppComponent
   */
  personList = [];

  /**
   * The url to request 10 male from the API
   *
   * @memberof AppComponent
   */
  requestMenAPI = 'https://randomuser.me/api/?gender=male&results=10';

  /**
   * The url to request 10 female from the API
   *
   * @memberof AppComponent
   */
  requestWomanAPI = 'https://randomuser.me/api/?gender=female&results=10';


  /**
   * When user toggles between
   * male or female
   * we store the selected gender
   * in this variable. When user then presses
   * "add persons" button ,this variable is
   * received as parameter in fetchData() function (button)
   *
   * @type {string}
   * @memberof AppComponent
   */
  menOrWomanToggle = this.requestMenAPI;

  /**
   * When the applications starts
   * a request of 10 male is made.
   * When toggle is set to female for the very
   * first time, 10 female is requested
   *
   * @memberof AppComponent
   */
  requestWoman = true;

  constructor(private http: HttpClient,
    private personListService: PersonListsService,
    private fetchingAlertService: FetchingAlertService) {
  }

  /**
   * When the app initialized 10 person is listed
   *
   * @memberof AppComponent
   */
  ngOnInit(): void {
      this.onFetchData();
  }

  /**
   * 
   * Request the first 10 person to the initial list
   * 
   *
   * @memberof AppComponent
   */
  onFetchData() {
    this.fetchData(this.requestMenAPI);
  }

  /**
   * When user toggles between gender
   * this method receives the gender in order
   * to make the next API request with the current view gender
   *
   * @param {*} data
   * @memberof AppComponent
   */
  toggleFromList(data: string) {
    if(this.requestWoman) {
      this.fetchData(this.requestWomanAPI)
      this.requestWoman = false;
    }
    if (data === 'male') {
      this.menOrWomanToggle = this.requestMenAPI;
    } else if ( data === 'female') {
      this.menOrWomanToggle = this.requestWomanAPI;
    }
  }

  /**
   * Request data to the API
   *
   * @param {*} reqType
   * @memberof AppComponent
   */
  public fetchData(reqType) {
    this.fetchingAlertService.alertFetching('fetching');
    this.http.get<any>(reqType)
    .pipe(
      map(responseData => {
        // const resultsArray: any[] = [];
        const resultsArray: Array<Person> = [];
        for(const key in responseData.results) {
          if(responseData.results.hasOwnProperty(key)) {
            resultsArray.push(
              {
                ...responseData.results[key].name, 
                gender: responseData.results[key].gender,
                thumbnail: responseData.results[key].picture.thumbnail,
                id: responseData.info.seed
              }
            );
          }
        }
        return resultsArray;
      }) 
    )
    .subscribe(data => {
      this.personList.push(data);
      this.personListService.updatePersonList(this.personList);
    });
  }
}


interface Person {
    gender: string,
    name: {
      title: string,
      first: string,
      last: string,
    },
    thumbnail: string
}