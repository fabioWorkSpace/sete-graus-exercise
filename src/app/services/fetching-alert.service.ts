import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchingAlertService {

  fecthingValue$: Observable<any>;

  public fetchingSubject = new Subject<any>();

  constructor() {
      this.fecthingValue$ = this.fetchingSubject.asObservable();
  }

  alertFetching(data) {
    this.fetchingSubject.next(data);
  }
}
