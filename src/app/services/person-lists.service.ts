import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonListsService {
  
    personListValue$: Observable<any>;

    public personValueSubject = new Subject<any>();

    constructor() {
        this.personListValue$ = this.personValueSubject.asObservable();
        
    }

    updatePersonList(data): any {
      this.personValueSubject.next(data);
    } 
}
