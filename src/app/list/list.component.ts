import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';

import { animateUsersList } from '../animations';
import { FetchingAlertService } from '../services/fetching-alert.service';
import { PersonListsService } from '../services/person-lists.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [animateUsersList]
})
export class ListComponent implements OnInit, OnDestroy {

  @Output() genderChanged = new EventEmitter<string>();

  /**
   * Person list array
   *
   * @memberof ListComponent
   */
  personLists: any[] = [];

  /**
   * Person List Service subscription
   *
   * @memberof ListComponent
   */
  personListSubscription = new Subscription;

  /**
   * Fetching data activates the spinner while fetching
   *
   * @memberof ListComponent
   */
  fetchingDataSubscription = new Subscription;

  /**
   * Initial gender to load 10 person
   *
   * @memberof ListComponent
   */
  gender = 'male';

  /**
   * Inicial spinner status
   *
   * @memberof ListComponent
   */
  fetchingData = true;

  /**
   * Angular animation initial state
   *
   * @memberof ListComponent
   */
  state = 'normal'
  
  constructor(private personListService: PersonListsService,
    private fetchinAlertService: FetchingAlertService) { }

  ngOnInit(): void {
    this.personListSubscription = this.personListService.personListValue$.subscribe((data) => {
      this.fetchingData = true;
      setTimeout(() => {
        this.personLists = data.flat();
        this.fetchingData = false;
      }, 2000);
    });

    this.fetchingDataSubscription = this.fetchinAlertService.fecthingValue$.subscribe((data) => {
      if (data === 'fecthing') {
        this.fetchingData = true;
      } else {
        return;
      }
    })
  }

  /**
   * Toggle between the gender male female
   *
   * @memberof ListComponent
   */
  toggleGender() {
    this.gender === 'male' ? this.gender = 'female' : this.gender = 'male';
    this.genderChanged.emit(this.gender);
  }

  /**
   * Unsubscribe to avoid memory leaks
   *
   * @memberof ListComponent
   */
  ngOnDestroy(): void {
    this.personListSubscription.unsubscribe();
    this.fetchingDataSubscription.unsubscribe();
  }
}
